# Acidgame.fr

Dépot AcidGame.fr - crée en octobre 2019


## Etat d'avancement

- [x] Créer le dépôt Github
- [ ] Créer le design du site et des différentes pages
- [ ] Intégration du design
- [ ] Développement 
- [ ] Déploiement

## Objectifs planifiés

- Réaliser le site web en PHP via le framework Laravel
- Créer une API qui permet de charger que les données liés aux articles et/ou dossiers


## Design

Pour le design j'utilise [Figma](https://www.figma.com) car c'est l'outil le plus simple à utiliser pour collaborer rapidement.

- [Maquettes Figma](https://www.figma.com/file/HnamCOnYf7eWZCtRIru5o1/Site?node-id=17%3A2)